package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UnitTest {

	@Test
    public void testAboutCPDOF() throws Exception {
		String expected = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.";
        String result = new AboutCPDOF().desc();
        assertEquals("Desc", expected, result);
	}

	@Test
    public void testDuration() throws Exception {
		String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        String result = new Duration().dur();
        assertEquals("Desc", expected, result);
	}

	@Test
    public void testMinMax() throws Exception {
		int expectedMax = 2;
		int resultMax = new MinMax().max(1, 2);
		assertEquals("Max", expectedMax, resultMax);
	}

	@Test
    public void testUsefulness() throws Exception {
		int expectedMax = 2;
		int resultMax = new MinMax().max(1, 2);
		assertEquals("Max", expectedMax, resultMax);
	}

}
