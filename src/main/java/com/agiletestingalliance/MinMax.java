package com.agiletestingalliance;

public class MinMax {

	public int max(int firstVal, int secondVal) {
		if (secondVal > firstVal) {
			return secondVal;
    }
		else {
			return firstVal;
    }
	}

	
	public String bar(String string) {
		if (string!=null && !string.equals("")) {
			return string;
    }
		return string;
	}

}
